# Copyright Michele Vidotto 2014 <michele.vidotto@gmail.com>

context prj/aln_ref

SAMPLES ?=
REV_COMP ?= FALSE
MAX_X ?= 0
MAX_Y ?= 0
# remember to change it in order to detect coordinate from read header
# and calculate optical duplicates. Default is:
READ_NAME_REGEX ?= [a-zA-Z0-9]+:[0-9]:([0-9]+):([0-9]+):([0-9]+).*
MIN_IDENTICAL_BASES ?= 25
MAX_GROUP_RATIO ?=50000

log:
	mkdir -p $@

tmp:
	mkdir -p $@

# this function install all the links at once
1_FASTQ_GZ = $(addsuffix .1.fastq.gz,$(SAMPLES))
2_FASTQ_GZ = $(addsuffix .2.fastq.gz,$(SAMPLES))
LB = $(addsuffix .lib,$(SAMPLES))
# this function install all the links at once
%.lib %.fastq.gz:
	@echo installing links ... \
	$(foreach SAMPLE,$(SAMPLES), \
		$(foreach KEY,$(call keys,$(SAMPLE)), \
			$(shell ln -sf $(call get,$(SAMPLE),$(KEY)) $(SAMPLE).$(KEY).fastq.gz) \
			$(shell echo -e '$(call get,$(SAMPLE),LB)' >$(SAMPLE).lib) \
		) \
	) \
	&& sleep 3


# for mate-pairs
ifeq ($(REV_COMP),TRUE)
1_FASTQ_REVCOMP_GZ = $(addsuffix .1.fastq.revcomp.gz,$(SAMPLES))
2_FASTQ_REVCOMP_GZ = $(addsuffix .2.fastq.revcomp.gz,$(SAMPLES))
%.fastq.revcomp.gz:  %.fastq.gz
	$(load_modules); \
	zcat <$< \
	| fastx_reverse_complement -Q 33 -z >$@
endif


UNALIGNED_BAM = $(addsuffix .unaligned.bam,$(SAMPLES))

ifeq ($(REV_COMP),TRUE)
%.unaligned.bam: log tmp %.lib %.1.fastq.revcomp.gz %.2.fastq.revcomp.gz
else
%.unaligned.bam: log tmp %.lib %.1.fastq.gz %.2.fastq.gz
endif
	$(call load_modules); \
	java -Xmx2g \
	-XX:MaxPermSize=512m \
	-XX:ParallelGCThreads=4 \
	-Djava.io.tmpdir=$^2 \
	-jar $$PICARDTOOLS_ROOT/picard.jar \
	FastqToSam \
	VALIDATION_STRINGENCY=SILENT \
	TMP_DIR=$^2 \
	SAMPLE_NAME="$*" \
	LIBRARY_NAME="$$(cat $^3)" \
	FASTQ=$^4 \
	FASTQ2=$^5 \
	OUTPUT=$@ \
	2>&1 \
	| tee $</FastqToSam.$(basename $@).log


COMPLEXITY_TXT = $(addsuffix .complexity.txt,$(SAMPLES))
%.complexity.txt: log tmp %.unaligned.bam
	$(call load_modules); \
	java -Xmx2g \
	-XX:MaxPermSize=512m \
	-XX:ParallelGCThreads=4 \
	-Djava.io.tmpdir=$^2 \
	-jar $$PICARDTOOLS_ROOT/picard.jar \
	EstimateLibraryComplexity \
	TMP_DIR=$^2 \
	VALIDATION_STRINGENCY=SILENT \
	MIN_IDENTICAL_BASES=$(MIN_IDENTICAL_BASES) \
	MAX_GROUP_RATIO=$(MAX_GROUP_RATIO) \
	INPUT=$^3 \
	OUTPUT=$@ \
	2>&1 \
	| tee $</EstimateLibraryComplexity.$(basename $@).log

ADUPLICATES_TXT = $(addsuffix .aduplicates.txt,$(SAMPLES))
ifeq ($(REV_COMP),TRUE)
%.aduplicates.txt: log %.1.fastq.revcomp.gz %.2.fastq.revcomp.gz
else
%.aduplicates.txt: log %.1.fastq.gz %.2.fastq.gz
endif
	$(call load_modules); \
	apriori_duplicates_filtering_v0.2 \
	-1 $^2 \
	-2 $^3 \
	2>&1 \
	| tee $</apriori_duplicates_filtering_v0.2.$(basename $@).log


BBCOUNT_TXT = $(addsuffix .bbcount.txt,$(SAMPLES))
ifeq ($(REV_COMP),TRUE)
%.bbcount.txt: log %.1.fastq.revcomp.gz %.2.fastq.revcomp.gz
else
%.bbcount.txt: log %.1.fastq.gz %.2.fastq.gz
endif
	$(call load_modules); \
	bbcountunique.sh \
	in=$^2 \
	in2=$^3 \
	out=$@ \
	k=25 \
	cumulative=t \
	printlastbin=t \
	overwrite=t \
	count=f \
	percent=t \
	2>&1 \
	| tee $</bbcountunique.$(basename $@).log

BBCOUNT_TMP = $(addsuffix .bbcount.tmp,$(SAMPLES))
%.bbcount.tmp: %.bbcount.txt
	sed '/^$$/d' $< \
	| bawk '!/^[\#+,$$]/ { last=$$(NF) } END { printf "$*\t%.6f\n", 1-(last/100); }' >$@


CENSUS = $(addsuffix .census,$(SAMPLES))
%.census: %.complexity.txt
	bawk '/duplication_group_count/,/^\s*$$/ { \   * extract diagram *
	print $$0; \
	}' $< \
	| unhead \
	| sed '/^$$/d' \
	| tac \   * reverse values from last to first *
	| calculate_libsize --mincount 1 --maxcount 10000 --start_prediction 0 --end_prediction 100 --step_prediction 5 - \
	| sed '1s/^/$*\n/' \
	| tee $@


UPRED = $(addsuffix .unique.pred.pdf,$(SAMPLES))
%.unique.pred.pdf: %.census
	bawk '/Prediction of observed unique molecules with given read counts/{flag=1;next} \   * Initial pattern found --> turn on the flag and read the next line *
	/^\s*$$/{flag=0} \   * Final pattern found   --> turn off rhe flag *
	flag { print $$0; }' $< \   * Flag on --> print the current line *
	| sed -e 's/^[ \t]*//' \
	| prediction_plot.R -t "$*" -X $(MAX_X) -Y $(MAX_Y) -o $@

.META: all.complexity.txt
	1	FILE_NAME
	2	PERCENT_DUPLICATION (BBCOUNT)
	3	PERCENT_DUPLICATION (PICARD)
	4	READS_TO_REACH_0.8_UNIQ_FRAC_(NB)
	5	READS_TO_REACH_0.8_UNIQ_FRAC_(LSD)
	6	PRED_FRAC_OBSERVED_NB_MODEL
	7	PRED_FRAC_OBSERVED_POISSON_MODEL
	8	NB_CV
	9	READ_PAIRS_EXAMINED
	10	READ_PAIR_DUPLICATES
	11	READ_PAIR_OPTICAL_DUPLICATES
	12	ESTIMATED_LIBRARY_SIZE
	13	LIBRARY
	14	UNPAIRED_READS_EXAMINED
	15	UNMAPPED_READS
	16	UNPAIRED_READ_DUPLICATES


all.complexity.txt: $(COMPLEXITY_TXT) $(CENSUS) $(BBCOUNT_TMP)
	grep -w --after-context 1 --with-filename 'LIBRARY' $(COMPLEXITY_TXT) \
	| grep -v -w 'LIBRARY' \
	| sed -e 's/txt-/txt\t/' \
	| sed -e '/--/d' -e 's/.complexity.txt//' \
	| translate -a <(cat $(BBCOUNT_TMP)) 1 \
	| translate -a \
	<(bawk 'BEGIN { print "FILE_NAME\tNB_CV\tPRED_FRAC_OBSERVED_POISSON_MODEL\tPRED_FRAC_OBSERVED_NB_MODEL\tREADS_TO_REACH_0.8_UNIQ_FRAC_(NB)\tREADS_TO_REACH_0.8_UNIQ_FRAC_(LSD)"; } !/^[\#+,$$]/ { \
	split(FILENAME, file_name, "."); \
	if ( $$0 ~ /NB CV:/ ) printf "%s\t%s\t", file_name[1], $$3; \
	if ( $$0 ~ /Predicted fraction of library observed, Poisson model:/ ) printf "%s\t", $$3; \
	if ( $$0 ~ /Predicted fraction of library observed, NB model:/ ) printf "%s\t", $$3; \
	if ( $$0 ~ /0.80:/ ) printf "%s\t%s\n", $$5, $$6; \
	 }' $(CENSUS)) \
	1 \
	| select_columns 1 7 15 5 6 4 3 2 10 13 14 16 8 9 11 12 >$@

#	| select_columns 1 14 5 6 4 3 2 9 12 13 15 7 8 10 11 >$@


# all.census: $(CENSUS)
	# bawk 'BEGIN { print "FILE_NAME\tNB_CV\tPRED_FRAC_OBSERVED_POISSON_MODEL\tPRED_FRAC_OBSERVED_NB_MODEL\tREADS_TO_REACH_0.8_UNIQ_FRAC_(NB)\tREADS_TO_REACH_0.8_UNIQ_FRAC_(LSD)"; } !/^[\#+,$$]/ { \
	# split(FILENAME, file_name, "."); \
	# if ( $$0 ~ /NB CV:/ ) printf "%s\t%s\t", file_name[1], $$3; \
	# if ( $$0 ~ /Predicted fraction of library observed, Poisson model:/ ) printf "%s\t", $$3; \
	# if ( $$0 ~ /Predicted fraction of library observed, NB model:/ ) printf "%s\t", $$3; \
	# if ( $$0 ~ /0.80:/ ) printf "%s\t%s\n", $$5, $$6; \
	 # }' $^ >$@

.PHONY: test
test:
	@echo

ALL +=  log \
	$(COMPLEXITY_TXT) \
	$(CENSUS) \
	$(UPRED) \
	$(BBCOUNT_TXT) \
	all.complexity.txt


INTERMEDIATE += $(UNALIGNED_BAM) \
		$(1_FASTQ_REVCOMP_GZ) \
		$(2_FASTQ_REVCOMP_GZ) \
		$(BBCOUNT_TMP) \
		tmp

CLEAN += $(UNALIGNED_BAM)
